class Timestamp:
    @staticmethod
    def time_stamp(min_value, max_value, timestamplist, columnlist):
        required_data = {}
        for index in range(len(columnlist)):
            if min_value == columnlist[index]:
                required_data["minimumTimestamp"] = timestamplist[index]
            if max_value == columnlist[index]:
                required_data["maximumTimestamp"] = timestamplist[index]
        return required_data
