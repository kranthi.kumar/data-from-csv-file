import pandas as pd

from scripts.core.min_max import Calculation
from scripts.core.send_mail import Send_Mail
from scripts.core.Csvfilecreation import Csv_file

data = pd.read_csv('input.csv')
required_data = ""
calculation_object = Calculation()

send_mail_object = Send_Mail()
message = calculation_object.calculation(data)
data1=Csv_file().csv_file(message)
print(data1)
send_mail_object.send_mail(data1)
